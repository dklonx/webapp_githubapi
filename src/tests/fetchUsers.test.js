import fetchUsers from '../modules/fetchUsers.js';

test('cerca user dklonx, verifica id', () => {
    fetchUsers('dklonx').then(res => {
        if(res.status == 200){
            
            expect(res.data.items[0].id).toBe(39165049);
        }
        
    });
});

test('cerca user torvalds, verifica id', () => {
    fetchUsers('torvalds').then(res => {

        if(res.status == 200){
            expect(res.data.items[0].id).toBe(1024025);
            
        }
    })
})