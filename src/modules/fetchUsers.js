import axios from 'axios';

const fetchUsers = (name) => {
    const url = 'https://api.github.com/search/users?q=' + name + "in:login in:name";
    const results =  axios.get(url)
        .then(res => {
            return res;
        })


    return results;
}

// console.log(await fetchUsers('torvalds'));
export default fetchUsers;