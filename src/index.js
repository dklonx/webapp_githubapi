
import fetchUsers from "./modules/fetchUsers.js";

const inputText = document.getElementById('inputText');
const btnSearch = document.getElementById('btnSearch');
const divResult = document.getElementById("divResult");
const detailContainer = document.getElementById("detailContainer");

btnSearch.addEventListener('click', searchResults);


function searchResults(){
    let loader = `<div id="loading"></div>`;


    const currentDate = new Date();

    const currentDayOfMonth = currentDate.getDate();
    const currentMonth = currentDate.getMonth();
    const currentYear = currentDate.getFullYear();

    const time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();

    const dateString = currentDayOfMonth + "-" + (currentMonth + 1) + "-" + currentYear + " alle " + time;

    let new_data =inputText.value;

    if(localStorage.getItem('keywords') == null){
        localStorage.setItem('keywords', '[]');
    }

    let old_data = JSON.parse(localStorage.getItem('keywords'));

    new_data = "hai cercato " +  new_data + " il " +  dateString;
    old_data.push(new_data);

    localStorage.setItem('keywords', JSON.stringify(old_data));

    divResult.innerHTML = loader;

    if(inputText.value){
        fetchUsers(inputText.value).then(res => {
            if(res.status == 200){
                console.log('caricamento riuscito');
                console.log(res.headers.link);

                if(res.headers.link){
                    const link = res.headers.link;
                    const links = link.split(',');

                    const urls = links.map(a => {
                        return {
                            url: a.split(';')[0].replace('<','').replace('>', ''),
                            title: a.split(';')[1]
                        }
                    });

                    showResults(res.data.items, urls);

                }else{
                    showResults(res.data.items, null);
                }
            }else{
                console.log('non trovato')
            }
        })
    }else{
        console.log('barra di ricerca vuota');
        divResult.innerHTML = '';
    }
    
}


function showResults(array, urls){
    clear();
    console.log(array);
    array.forEach(a => {
        let div = document.createElement('div');
        div.style.border = "2px solid red";
        
        const img = document.createElement('img');
        img.src = a.avatar_url;
        img.style.width = "32px";
        img.style.height = "32px";

        const name = document.createElement('span');
        name.textContent = a.login;

        const id = document.createElement('span');
        id.textContent = a.id + " - ";
        div.append(img);
        div.append(id);
        div.append(name);


        div.classList.add('utente');
        div.setAttribute('url_user', a.url)


        const saveBtn = document.createElement('button');
        saveBtn.classList.add('saveUser');
        saveBtn.style.background = 'black';
        saveBtn.style.color = 'white';
        saveBtn.textContent = 'save user'


        saveBtn.setAttribute('user_data', JSON.stringify(a))


        const unsaveBtn = document.createElement('button');
        unsaveBtn.classList.add('unsaveUser');
        unsaveBtn.style.background = 'red';
        unsaveBtn.style.color = 'black';
        unsaveBtn.textContent = 'unsave user'
        
        unsaveBtn.setAttribute('user_data', JSON.stringify(a))
        
        let old_data = JSON.parse(localStorage.getItem('favorites'));
        console.log('verifica array favorites');
        let trovato = false;
        if(old_data){
            for(let i = 0; i < old_data.length && trovato == false; i++){
                if(old_data[i].id === a.id){
                    console.log(a.login, old_data[i].login)
                    saveBtn.style.display = 'none';
                    unsaveBtn.style.display = 'initial';
                    trovato = true;
                }
                else{
                    
                    unsaveBtn.style.display = 'none';
                    saveBtn.style.display = 'initial';
    
                }
            }
        }else{
            unsaveBtn.style.display = 'none';
            saveBtn.style.display = 'initial';
        }


        div.append(saveBtn);
        div.append(unsaveBtn);


        divResult.append(div);

    });


    addSaveEvent();
    addUnsaveEvent();
    addDetailsEvent();

    if(urls){
        urls.forEach(u=>{
            const btn = document.createElement('button');
            btn.textContent = u.title;
            btn.addEventListener('click', () => {
                showUsers(u.url);
            })

            divResult.append(btn);
        });
    }

}



function showUsers(url){
    console.log("url = " ,url)
    let loader = `<div id="loading"></div>`;
    divResult.innerHTML = loader;
    
    fetch(url).then(res => {

        let link = res.headers.get('link');
        console.log(link);

        let links = link.split(',');
        console.log(links);

        const urls = links.map(a=>{
            return {
                url: a.split(';')[0].replace('<','').replace('>','').replace('{?since}',''),
                title: a.split(';')[1].replace('rel=','').replace('"','').replace('"', '')
            }
        })

        console.log(urls)

        if(res.status == 200){
            console.log("caricato correttamente");
            res.json().then(r=>{
                console.log('brother',r.total_count)
                showResults(r.items, urls)
            })
        }
    })
}


function clear(){

    // non utilizzo .innerHTML xche troppo lento meglio utilizzare while 

    while(divResult.firstChild)
        divResult.removeChild(divResult.firstChild);
}

function addSaveEvent(){
    let saveBtns = document.querySelectorAll('.saveUser');
    Array.from(saveBtns).forEach( b => {
        b.addEventListener('click', saveTheUser)
    });
}

function addUnsaveEvent(){
    let unsaveBtns = document.querySelectorAll('.unsaveUser');
    Array.from(unsaveBtns).forEach( b => {
        b.addEventListener('click', unsaveTheUser)
    });
}


function addDetailsEvent(){
    let clickableUser = document.querySelectorAll('.utente'); 
    Array.from(clickableUser).forEach( c => {
        c.addEventListener('click', showDetails);
    })
}

function showDetails(){
    console.log('ehi bro u clicked a user')
    console.log(this.getAttribute('url_user'));

    
    let url = this.getAttribute('url_user');
    console.log('url from object', url);
    const fullName = fetch(url).then(res => {
       res.json().then(res => {

            const div = document.createElement('div');
            
            const login = document.createElement('span');
            login.textContent = "login = " + res.login;

            const id = document.createElement('span');
            id.textContent = "id = " + res.id;
            const name = document.createElement('span');
            name.textContent = "name = " + res.name;
            const img = document.createElement('img');
            img.src = res.avatar_url;
            img.style.height = "50px";
            img.style.width = "50px";

            div.append(login,document.createElement('br'));
            div.append(id, document.createElement('br'));

            div.append(name, document.createElement('br'));
            div.append(img, document.createElement('br'));
            
            detailContainer.innerHTML = '';
            detailContainer.append(div);
       });
        
    })


}


function saveTheUser(){
    let obj = JSON.parse(this.getAttribute('user_data'));
    console.log(obj);
    
    if(localStorage.getItem('favorites') == null){
        localStorage.setItem('favorites', '[]');
    }

    let old_data = JSON.parse(localStorage.getItem('favorites'));

    old_data.push(obj);
    

    localStorage.setItem('favorites', JSON.stringify(old_data));
    console.log('saveuserold data',old_data)
    this.style.display = 'none';

    this.nextElementSibling.style.display = 'initial';

}


function unsaveTheUser(){
    let obj = JSON.parse(this.getAttribute('user_data'));
    console.log(obj);
    let old_data = JSON.parse(localStorage.getItem('favorites'));
    console.log('unsave user old data',old_data)
    

    console.log('obj id ',obj.id)
    for(let i = 0; i < old_data.length; i++){
        console.log('old data id',old_data[i].id);
        if(obj.id === old_data[i].id){
            old_data.splice(i, 1);
            i--;
        }
    }

    localStorage.setItem('favorites', JSON.stringify(old_data));
    this.style.display = 'none';

    this.previousElementSibling.style.display = 'initial';
}





