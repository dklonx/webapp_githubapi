const fieldset = document.querySelector('fieldset');
const clearBtn = document.querySelector('#clear');


clearBtn.addEventListener('click', () => {
    localStorage.clear();
    fieldset.innerText = ' ';
})

console.log(localStorage)

function riempiStorico(){
    
    let keywords = JSON.parse(localStorage.getItem('keywords'));
    if(keywords){
        
        keywords.forEach(k => {
            const p = document.createElement('p');
            p.append(k);
            p.style.border = 'solid 2px red';
            fieldset.append(p);
        })
    }
    console.log(keywords);
}


function riempiPreferiti(){
    let array = JSON.parse(localStorage.getItem('favorites'));
    if(array){
        // console.log("array dei preferiti = ", array);
        // console.log(array)

        array.forEach(a => {
            let div = document.createElement('div');
            div.style.border = "2px solid red";
            
            const img = document.createElement('img');
            img.src = a.avatar_url;
            img.style.width = "32px";
            img.style.height = "32px";
    
            const name = document.createElement('span');
            name.textContent = a.login;
    
            const id = document.createElement('span');
            id.textContent = a.id + " - ";
            div.append(img);
            div.append(id);
            div.append(name);
    
    
            div.classList.add('utente');
            div.setAttribute('url_user', a.url)
    
    
            const saveBtn = document.createElement('button');
            saveBtn.classList.add('saveUser');
            saveBtn.style.background = 'black';
            saveBtn.style.color = 'white';
            saveBtn.textContent = 'save user'
    
    
            saveBtn.setAttribute('user_data', JSON.stringify(a))
    
    
            const unsaveBtn = document.createElement('button');
            unsaveBtn.classList.add('unsaveUser');
            unsaveBtn.style.background = 'red';
            unsaveBtn.style.color = 'black';
            unsaveBtn.textContent = 'unsave user'
            
            unsaveBtn.setAttribute('user_data', JSON.stringify(a))
            
            let old_data = JSON.parse(localStorage.getItem('favorites'));
            console.log('verifica array favorites');
            let trovato = false;
            if(old_data){
                for(let i = 0; i < old_data.length && trovato == false; i++){
                    if(old_data[i].id === a.id){
                        console.log(a.login, old_data[i].login)
                        saveBtn.style.display = 'none';
                        unsaveBtn.style.display = 'initial';
                        trovato = true;
                    }
                    else{
                        
                        unsaveBtn.style.display = 'none';
                        saveBtn.style.display = 'initial';
        
                    }
                }
            }else{
                unsaveBtn.style.display = 'none';
                saveBtn.style.display = 'initial';
            }
    
    
            div.append(saveBtn);
            div.append(unsaveBtn);
    
    
            divFavorites.append(div);
    
        });
    
    
        addSaveEvent();
        addUnsaveEvent();
        addDetailsEvent();
    }
}


function addSaveEvent(){
    let saveBtns = document.querySelectorAll('.saveUser');
    Array.from(saveBtns).forEach( b => {
        b.addEventListener('click', saveTheUser)
    });
}

function addUnsaveEvent(){
    let unsaveBtns = document.querySelectorAll('.unsaveUser');
    Array.from(unsaveBtns).forEach( b => {
        b.addEventListener('click', unsaveTheUser)
    });
}


function addDetailsEvent(){
    let clickableUser = document.querySelectorAll('.utente'); 
    Array.from(clickableUser).forEach( c => {
        c.addEventListener('click', showDetails2);
    })
}

function saveTheUser(){
    let obj = JSON.parse(this.getAttribute('user_data'));
    console.log(obj);
    
    if(localStorage.getItem('favorites') == null){
        localStorage.setItem('favorites', '[]');
    }

    let old_data = JSON.parse(localStorage.getItem('favorites'));

    old_data.push(obj);
    

    localStorage.setItem('favorites', JSON.stringify(old_data));
    console.log('saveuserold data',old_data)
    this.style.display = 'none';

    this.nextElementSibling.style.display = 'initial';

}


function unsaveTheUser(){
    let obj = JSON.parse(this.getAttribute('user_data'));
    console.log(obj);
    let old_data = JSON.parse(localStorage.getItem('favorites'));
    console.log('unsave user old data',old_data)
    

    console.log('obj id ',obj.id)
    for(let i = 0; i < old_data.length; i++){
        console.log('old data id',old_data[i].id);
        if(obj.id === old_data[i].id){
            old_data.splice(i, 1);
            i--;
        }
    }

    localStorage.setItem('favorites', JSON.stringify(old_data));
    this.style.display = 'none';

    this.previousElementSibling.style.display = 'initial';
}

function showDetails2(){
    console.log('ehi bro u clicked a user')
    console.log(this.getAttribute('url_user'));

    
    let url = this.getAttribute('url_user');
    console.log('url from object', url);
    const fullName = fetch(url).then(res => {
       res.json().then(res => {

            const div = document.createElement('div');
            
            const login = document.createElement('span');
            login.textContent = "login = " + res.login;

            const id = document.createElement('span');
            id.textContent = "id = " + res.id;
            const name = document.createElement('span');
            name.textContent = "name = " + res.name;
            const img = document.createElement('img');
            img.src = res.avatar_url;
            img.style.height = "50px";
            img.style.width = "50px";

            div.append(login,document.createElement('br'));
            div.append(id, document.createElement('br'));

            div.append(name, document.createElement('br'));
            div.append(img, document.createElement('br'));
            
            divDetailsFavorites.innerHTML = '';
            divDetailsFavorites.append(div);
       });
        
    })


}

riempiStorico();
riempiPreferiti();